# clickmeeting recruitment

Należy zaimplementować w technologii PHP mechanizm tworzenia miniaturek obrazków z lokalnego dysku i zapisywania ich we wskazanym miejscu. Rozmiar dłuższego boku obrazka po przeskalowaniu to maksymalnie 150px. Miejscem docelowym zapisywanych plików, w zależności od wyboru użytkownika, może być folder na dysku, bucket w usłudze Amazon S3 lub folder w usłudze Dropbox. 

Proszę dołączyć instrukcję uruchomieniową oraz opis rozwiązania.

## Installation

* Clone repository to your `projects` directory.
* Edit `<project_name>/php/app/.env` file and fill those values.
```
DROPBOX_APP_TOKEN=
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
```
* Go to `<project_name>/docker/logs/nginx` and remove `.dist` extension from both files.
* Now, you can run the app. Open terminal, go to the project directory and use below commands:
```
docker-compose up -d --build
```
```
docker exec -it <fpm_container_name> sh
> composer install
> yarn install
> yarn build
> exit
```
OK. That's it! Open your browser on `localhost` page and start uploading yours images.
