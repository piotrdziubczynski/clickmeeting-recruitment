<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 27.05.2020
 * Time: 18:26
 */

namespace App\Form\Type;

use App\DataTransferObject\UploadDto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UploadType
 * @package App\Form\Type
 */
final class UploadType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'image',
                FileType::class,
                [
                    'attr' => [
                        'accept' => 'image/gif,image/jpeg,image/png',
                    ],
                    'help' => 'form.upload.max_size_and_accepted_files',
                    'label' => 'form.upload.choose.file',
                ]
            )
            ->add(
                'storage',
                ChoiceType::class,
                [
                    'choices' => $options['storages'],
                    'choice_label' => 'name',
                    'choice_translation_domain' => 'storage',
                    'label' => 'form.upload.choose.storage',
                ]
            )
            ->add(
                'upload',
                SubmitType::class,
                [
                    'attr' => [
                        'class' => 'btn-warning float-right',
                    ],
                    'label' => 'form.upload.button',
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => UploadDto::class,
                'allow_file_upload' => true,
            ]
        );

        $resolver->setRequired('storages');
        $resolver->setAllowedTypes('storages', 'App\\Service\\StorageInterface[]');
    }
}