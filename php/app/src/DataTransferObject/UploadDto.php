<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 27.05.2020
 * Time: 19:28
 */

namespace App\DataTransferObject;

use App\Service\StorageInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UploadDto
 * @package App\DataTransferObject
 */
final class UploadDto
{
    /**
     * @Assert\NotBlank()
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={"image/gif", "image/jpeg", "image/png"}
     * )
     */
    public ?UploadedFile $image = null;

    /**
     * @Assert\NotNull()
     * @Assert\Type("App\Service\StorageInterface")
     */
    public ?StorageInterface $storage = null;
}