<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 27.05.2020
 * Time: 15:19
 */

namespace App\Controller;

use App\DataTransferObject\UploadDto;
use App\Form\Type\UploadType;
use App\Service\Manager\Resizr\ImageResizr;
use App\Service\Storage\AwsStorage;
use App\Service\Storage\DropboxStorage;
use App\Service\Storage\LocalStorage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 */
final class HomeController extends AbstractController
{
    /**
     * @Route("/")
     * @Route("/home", name="homepage")
     *
     * @param Request $request
     * @param string $projectDir
     * @param string $dropboxToken
     *
     * @return Response
     */
    public function index(
        Request $request,
        string $projectDir,
        string $dropboxToken
    ): Response
    {
        $form = $this->createForm(
            UploadType::class,
            new UploadDto(),
            [
                'storages' => [
                    new LocalStorage($projectDir),
                    new DropboxStorage($dropboxToken),
                    new AwsStorage(),
                ],
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadDto $data */
            $data = $form->getData();

            $resizr = new ImageResizr($data->image);
            $resizr->setMaxWidthOrHeight(150);
            $resizr->resize();

            $location = $data->storage->save($resizr->getFile());

            $this->addFlash('success', 'flashes.success.upload');
            return $this->redirectToRoute('homepage');
        }

        return $this->render(
            'home/index.html.twig',
            [
                'title' => 'Homepage',
                'form' => $form->createView(),
            ]
        );
    }
}