<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 27.05.2020
 * Time: 19:36
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface StorageInterface
 * @package App\Service
 */
interface StorageInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    public function save(UploadedFile $file): string;
}