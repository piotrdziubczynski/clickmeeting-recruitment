<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 27.05.2020
 * Time: 23:29
 */

namespace App\Service\Manager\Resizr;

use LogicException;
use RuntimeException;
use SplFileInfo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class ImageResizr
 * @package App\Service\Manager\Resizr
 */
final class ImageResizr
{
    private static array $allowedMimeTypes = ['image/gif', 'image/jpeg', 'image/png'];
    private UploadedFile $image;
    private ?string $mimeType;
    private int $maxWidthOrHeight;

    /**
     * ImageResizr constructor.
     *
     * @param UploadedFile $file
     */
    public function __construct(UploadedFile $file)
    {
        $this->image = $file;
        $this->mimeType = $this->image->getMimeType() ?? 'none';
        $this->maxWidthOrHeight = 1;
    }

    /**
     * @param int $width
     * @param int $height
     *
     * @return array|int[]
     */
    private function getNewWidthAndHeight(int $width, int $height): array
    {
        if ($width > $height) {
            $newWidth = $this->maxWidthOrHeight;
            $newHeight = (int)(round($newWidth * ($height / $width)) ?: 0);
        } else {
            $newHeight = $this->maxWidthOrHeight;
            $newWidth = (int)(round($newHeight * ($width / $height)) ?: 0);
        }

        return [$newWidth, $newHeight];
    }

    /**
     * @return UploadedFile
     */
    public function getFile(): UploadedFile
    {
        return $this->image;
    }

    /**
     * @return int
     */
    public function getMaxWidthOrHeight(): int
    {
        return $this->maxWidthOrHeight;
    }

    /**
     * @param int $pixels
     */
    public function setMaxWidthOrHeight(int $pixels): void
    {
        if ($pixels < 1) {
            throw new LogicException('This value cannot be smaller than one.');
        }

        $this->maxWidthOrHeight = $pixels;
    }

    public function resize(): void
    {
        if (!in_array($this->mimeType, self::$allowedMimeTypes, true)) {
            throw new RuntimeException(
                sprintf('File has not supported MIME type (%s).', $this->mimeType)
            );
        }

        [$type, $ext] = explode('/', $this->mimeType);
        $imageCreate = sprintf('imagecreatefrom%s', mb_strtolower($ext));
        $imageSave = sprintf('image%s', mb_strtolower($ext));

        [$width, $height] = array_values(getimagesize($this->image->getRealPath()));
        [$newWidth, $newHeight] = $this->getNewWidthAndHeight($width, $height);
        $canvas = imagecreatetruecolor($newWidth, $newHeight);
        $image = $imageCreate($this->image->getRealPath());

        imagecopyresampled($canvas, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        $imageSave($canvas, $this->image->getRealPath());
        imagedestroy($canvas);

        return;
    }
}