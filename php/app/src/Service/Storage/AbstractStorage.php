<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 28.05.2020
 * Time: 15:10
 */

namespace App\Service\Storage;

use App\Service\StorageInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AbstractStorage
 * @package App\Service\Storage
 */
abstract class AbstractStorage implements StorageInterface
{
    protected string $name;
    protected string $filename;

    /**
     * AbstractStorage constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->filename = uniqid();
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    protected function getNewFilename(UploadedFile $file): string
    {
        return sprintf('%s.%s', $this->filename, $file->getClientOriginalExtension());
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return sprintf('storage.%s', $this->name);
    }
}