<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 27.05.2020
 * Time: 20:54
 */

namespace App\Service\Storage;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LocalStorage
 * @package App\Service\Storage
 */
final class LocalStorage extends AbstractStorage
{
    private string $directory;

    /**
     * LocalStorage constructor.
     *
     * @param string $projectDir
     */
    public function __construct(string $projectDir)
    {
        parent::__construct('local');

        $this->directory = $projectDir . '/var/thumbnails';
    }

    /**
     * @inheritDoc
     */
    public function save(UploadedFile $file): string
    {
        $file->move($this->directory,$this->getNewFilename($file));

        return sprintf('%s/%s', $this->directory,$this->getNewFilename($file));
    }
}