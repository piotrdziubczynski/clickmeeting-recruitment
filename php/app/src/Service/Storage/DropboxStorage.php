<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 27.05.2020
 * Time: 20:54
 */

namespace App\Service\Storage;

use Spatie\Dropbox\Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class DropboxStorage
 * @package App\Service\Storage
 */
final class DropboxStorage extends AbstractStorage
{
    private Client $dropbox;

    /**
     * DropboxStorage constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        parent::__construct('dropbox');

        $this->dropbox = new Client($token);
    }

    /**
     * @inheritDoc
     */
    public function save(UploadedFile $file): string
    {
        $contents = file_get_contents($file->getRealPath());
        $response = $this->dropbox->uploadSessionFinish(
            $contents,
            $this->dropbox->uploadSessionStart($contents),
            sprintf('/thumbnails/%s', $this->getNewFilename($file))
        );

        return $response['path_display'] ?? '';
    }
}