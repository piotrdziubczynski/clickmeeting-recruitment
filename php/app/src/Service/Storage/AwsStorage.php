<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 27.05.2020
 * Time: 20:54
 */

namespace App\Service\Storage;

use Aws\Credentials\CredentialProvider;
use Aws\S3\S3Client;
use DateTimeImmutable;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AwsStorage
 * @package App\Service\Storage
 */
final class AwsStorage extends AbstractStorage
{
    private S3Client $s3;
    private string $bucketName;

    /**
     * AwsStorage constructor.
     */
    public function __construct()
    {
        parent::__construct('aws');

        $this->bucketName = 'clickmeeting-recruitment-' . (new DateTimeImmutable())->format('Y');
        $this->s3 = new S3Client(
            [
                'credentials' => CredentialProvider::env(),
                'region' => 'eu-west-2',
                'version' => 'latest',
            ]
        );
    }

    /**
     * @inheritDoc
     */
    public function save(UploadedFile $file): string
    {
        if (!$this->s3->doesBucketExist($this->bucketName)) {
            $this->s3->createBucket(
                [
                    'ACL' => 'authenticated-read',
                    'Bucket' => $this->bucketName,
                    'CreateBucketConfiguration' => [
                        'LocationConstraint' => 'eu-west-2',
                    ],
                ]
            );

            $this->s3->waitUntil(
                'BucketExists',
                [
                    'Bucket' => $this->bucketName,
                ]
            );
        }

        $result = $this->s3->putObject(
            [
                'ACL' => 'authenticated-read',
                'Body' => file_get_contents($file->getRealPath()),
                'Bucket' => $this->bucketName,
                'Key' => $this->getNewFilename($file),
            ]
        );

        return (string)$result->get('ObjectURL');
    }
}